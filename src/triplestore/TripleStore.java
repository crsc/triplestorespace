package triplestore;

public interface TripleStore {
    public void add(Triple triple);
    public void remove(Triple triple);

}
