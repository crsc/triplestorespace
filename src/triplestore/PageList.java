package triplestore;

import java.util.ArrayList;

public class PageList {
    public static final int MAX_PREDICATES = 1 << 10;
    
    private ArrayList<Page> pages;
    private Space[] spaces;
    
    public PageList() {
        this.pages = new ArrayList<>();
        this.spaces = new Space[MAX_PREDICATES];
    }

    public Page get(Triple triple) {
        Space space = loadSpace(triple.getPredicate());
        int index = space.getPageIndex(PageCoordinate.fromTriple(triple));
        if (index < 0) return null;
        return pages.get(index);
    }
    
    public Page load(Triple triple) {
        Page page = get(triple);
        if (page != null) return page;
        return createPage(loadSpace(triple.getPredicate()), PageCoordinate.fromTriple(triple));
    }
    
    private Space loadSpace(int predicate) {
        if (spaces[predicate] == null) spaces[predicate] = new Space();
        return spaces[predicate];
    }

    private Page createPage(Space space, PageCoordinate coordinate) {
        Page page  = new Page();
        pages.add(page);
        space.setPageIndex(coordinate, pages.indexOf(page));
        return page;
    }
    
}
