package triplestore;

public class Page {
    private static final int SUBJECT = 0;
    private static final int OBJECT = 1;
    
    private byte[][] cells;
    
    public Page() {
        this.cells = new byte[0][0];
    }
    
    public void add(byte subject, byte object) {
        if (check(subject, object)) return;
        extend();
        setCell(count() - 1, subject, object);
    }
    
    public void remove(byte subject, byte object) {
        byte index = 0;
        for (int i = 0; i < this.count(); i++) {
            if (checkCell(i, subject, object)) continue;
            copyCell(i, index);
            index++;
        }
        adjustSize(index);
    }
    
    public boolean check(byte subject, byte object) {
        for (int i = 0; i < count(); i++)
            if (checkCell(i, subject, object)) return true;
        return false;
    }

    private int count() {
        return this.cells.length;
    } 
    
    private void setCell(int index, byte subject, byte object) {
        cells[index][SUBJECT] = subject;
        cells[index][OBJECT] = object;
    }
    
    private boolean checkCell(int index, byte subject, byte object) {
        return (cells[index][SUBJECT] == subject) && (cells[index][OBJECT] == object);
    }
    
    private void copyCell(int src, int dst) {
        if (src == dst) return;
        cells[dst][SUBJECT] = cells[src][SUBJECT];
        cells[dst][OBJECT] = cells[src][SUBJECT];
    }

    private void extend() {
        adjustSize(count() + 1);
    }

    private void adjustSize(int size) {
        if (size == count()) return;
        byte[][] extension = new byte[size][2];
        System.arraycopy(this.cells, 0, extension, 0, Math.min(this.cells.length, size));
        this.cells = extension;
    }

}
