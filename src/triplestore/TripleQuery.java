package triplestore;

public interface TripleQuery {
    
    public boolean check(Triple triple);

}
