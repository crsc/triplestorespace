package triplestore;

public class PageCoordinate {

    private int x;
    private int y;

    public PageCoordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    
    public PageCoordinate add(PageCoordinate coordinate) {
        return new PageCoordinate(this.x + coordinate.x, this.y + coordinate.y);
        
    }
    public PageCoordinate substract(PageCoordinate coordinate) {
        return new PageCoordinate(this.x - coordinate.x, this.y - coordinate.y);
    }
    
    public boolean checkBoundaries(int size) {
        return (x >= 0) && (x < size) && (y >= 0) && (y < size);
    }
    
    public static PageCoordinate fromTriple(Triple triple) {
        return new PageCoordinate((int) triple.getSubject() >> 8, (int) triple.getObject() >> 8);
    }
    
    public boolean match(PageCoordinate coordinate) {
        return (coordinate.x == this.x) && (coordinate.y == this.y);
    }
    
}
