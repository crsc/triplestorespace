package triplestore;


public class ImageTripleStore implements TripleStore, TripleQuery {
    
    private PageList pageList;

    public ImageTripleStore() {
        this.pageList = new PageList();
    }

    @Override
    public void add(Triple triple) {
        Page page = pageList.load(triple);
        page.add((byte) triple.getSubject(), (byte) triple.getObject());
    }

    @Override
    public void remove(Triple triple) {
        Page page = pageList.get(triple);
        if (page == null) return;
        page.remove((byte) triple.getSubject(), (byte) triple.getObject());
    }

    @Override
    public boolean check(Triple triple) {
        Page page = pageList.get(triple);
        if (page == null) return false;
        return page.check((byte) triple.getSubject(), (byte) triple.getObject());        
    }

}
