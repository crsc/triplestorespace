package triplestore;

public class Region {    
    private PageCell[] cells;
    private final static int SIZE = 1 << 16;
    
    public Region() {
        this.cells = new PageCell[0];
    }

    public int get(PageCoordinate coordinate) {
        checkBoundaries(coordinate);
        PageCell cell = getCell(coordinate);
        if (cell == null) return 0;
        return cell.index;
    }
    
    public void set(PageCoordinate coordinate, int value) {
        checkBoundaries(coordinate);
        checkOwerride(coordinate);
        PageCell cell = createCell(coordinate);
        cell.index = value;
    }

    private PageCell getCell(PageCoordinate coordinate) {
        for (PageCell cell : cells)
            if (cell.match(coordinate)) return cell;
        return null;
    }
    
    private PageCell createCell(PageCoordinate coordinate) {
        extend();
        PageCell cell = new PageCell(coordinate);
        cells[cells.length - 1] = cell;
        return cell;
    }
    
    private void extend() {
        PageCell[] extension = new PageCell[cells.length + 1];
        System.arraycopy(cells, 0, extension, 0, cells.length);
        this.cells = extension;
    }

    private void checkOwerride(PageCoordinate coordinate) throws RuntimeException {
        if (getCell(coordinate) != null) 
            throw new RuntimeException("No se puede sobreescribir el valor");
    }

    private void checkBoundaries(PageCoordinate coordinate) {
        if (coordinate.checkBoundaries(SIZE)) return;
        throw new RuntimeException("Fuera de los limites permitidos");
    }

    private class PageCell {
        private PageCoordinate coordinate;
        private int index;
        
        private PageCell(PageCoordinate coordinate) {
            this.coordinate = coordinate;
        }
        
        public boolean match(PageCoordinate coordinate) {
            return coordinate.match(this.coordinate);
        }
    }
 

}
