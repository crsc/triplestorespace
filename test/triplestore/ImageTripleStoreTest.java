package triplestore;

import junit.framework.Assert;
import org.junit.Test;

public class ImageTripleStoreTest {

    public ImageTripleStoreTest() {
        
    }
    
    @Test
    public void addTriple() {
        ImageTripleStore store = new ImageTripleStore();
        store.add(new Triple(0,0,1));
        Assert.assertTrue(store.check(new Triple(0,0,1)));
        Assert.assertFalse(store.check(new Triple(0,0,0)));
        Assert.assertFalse(store.check(new Triple(0,1,0)));
        Assert.assertFalse(store.check(new Triple(1,0,0)));
    }
    
    @Test
    public void add2Triples() {
        ImageTripleStore store = new ImageTripleStore();
        store.add(new Triple(0,0,1));
        store.add(new Triple(0,0,2));
        Assert.assertFalse(store.check(new Triple(0,0,0)));
        Assert.assertFalse(store.check(new Triple(0,1,0)));
        Assert.assertTrue(store.check(new Triple(0,0,1)));
        Assert.assertTrue(store.check(new Triple(0,0,2)));
        Assert.assertFalse(store.check(new Triple(1,0,0)));
    }    
    
    @Test
    public void addAndRemoveTriple() {
        ImageTripleStore store = new ImageTripleStore();
        store.add(new Triple(0,0,1));
        store.remove(new Triple(0,0,1));
        Assert.assertFalse(store.check(new Triple(0,0,1)));
    }
    
    @Test
    public void add12Triples() {
        ImageTripleStore store = new ImageTripleStore();
        store.add(new Triple(0,0,0));
        store.add(new Triple(0,0,1));
        store.add(new Triple(0,1,2));
        store.add(new Triple(0,2,3));
        store.add(new Triple(0,2,4));
        store.add(new Triple(0,2,5));
        store.add(new Triple(1,0,1));
        store.add(new Triple(1,0,0));
        store.add(new Triple(1,1,2));
        store.add(new Triple(1,2,3));
        store.add(new Triple(1,2,4));
        store.add(new Triple(1,2,6));
        Assert.assertTrue(store.check(new Triple(0,0,0)));
        Assert.assertTrue(store.check(new Triple(0,0,1)));
        Assert.assertTrue(store.check(new Triple(0,1,2)));
        Assert.assertTrue(store.check(new Triple(0,2,3)));
        Assert.assertTrue(store.check(new Triple(0,2,4)));
        Assert.assertTrue(store.check(new Triple(0,2,5)));
        Assert.assertTrue(store.check(new Triple(1,0,1)));
        Assert.assertTrue(store.check(new Triple(1,0,0)));
        Assert.assertTrue(store.check(new Triple(1,1,2)));
        Assert.assertTrue(store.check(new Triple(1,2,3)));
        Assert.assertTrue(store.check(new Triple(1,2,4)));
        Assert.assertTrue(store.check(new Triple(1,2,6)));
        Assert.assertFalse(store.check(new Triple(1,0,6)));
        Assert.assertFalse(store.check(new Triple(0,4,2)));
        Assert.assertFalse(store.check(new Triple(1,3,8)));
    }
    
   @Test
    public void addTripleGreaterThan256() {
        ImageTripleStore store = new ImageTripleStore();
        store.add(new Triple(0,0,256));
        store.add(new Triple(1 << 20, 1 << 8, 1 << 22));
        Assert.assertFalse(store.check(new Triple(0,0,0)));
        Assert.assertTrue(store.check(new Triple(0,0,256)));
        Assert.assertTrue(store.check(new Triple(1 << 20, 1 << 8, 1 << 22)));
        Assert.assertFalse(store.check(new Triple(1 << 20, 1 << 8, 1 << 22 + 1)));
        Assert.assertFalse(store.check(new Triple(1 << 4, 1 << 2, 1 << 10)));
    }    

   @Test
    public void addTripleGreaterThan4Mega() {
        ImageTripleStore store = new ImageTripleStore();
        store.add(new Triple(0,0,1<<24));
        Assert.assertFalse(store.check(new Triple(0,0,0)));
        Assert.assertTrue(store.check(new Triple(0,0,1<<24)));
    }    



}